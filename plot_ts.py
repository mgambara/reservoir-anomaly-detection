import os
import pickle
import config as c
import numpy as np
import pandas as pd
from datetime import datetime
from datetime import timedelta
import util
import platform
import seaborn as sns
import matplotlib.backends.backend_pdf
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.gridspec as gsp
from matplotlib.ticker import AutoMinorLocator
from prediction_methods import load_reservoirs, open_associated_csv

logger = util.get_logger(__name__)
path2plot = os.getcwd() + c.plot_flags_pd
sns.set_style('darkgrid')
sns.set_context('paper')
fmt = "%Y-%m-%d %H:%M"


@util.print_runtime
def plot_flagged_interval_as_PD(method=''):
    ## NOTE: There is a limit of 12 intervals that is possible to plot_anomaly right now
    signatures = load_reservoirs(method=method)
    pdf = matplotlib.backends.backend_pdf.PdfPages(path2plot + 'output.pdf')
    i = 0
    for key, values in signatures.items():  # key is ".pl"-file name where signature is stored
        indeces2plot = np.array([ind for ind, interval in enumerate(values.deck) if interval.includePD])
        print(indeces2plot);
        print(len(indeces2plot))
        elements2plot = np.array(signatures[key].deck)[indeces2plot]
        df, csv_name = open_associated_csv(key)
        fig = validation_plots(name=csv_name[:-4], df=df, plot_elements=elements2plot, save=False, top=0.93)
        plt.suptitle(f'file {key}')
        pdf.savefig(fig)
        plt.close()
        i += 1
    pdf.close()
    logger.info(f'Produced pdf with {i} pages.')


@util.print_runtime
def plotOneFile(fileName, fileValues, moreMin=20, plotsPerPage=9):
    df, csv_name = open_associated_csv(pickle_file_name=fileName)
    pdDate = datetime.strptime(csv_name.split('_')[-1].replace('.csv', '').replace('.', ':'), fmt)
    indeces2plot = np.array([ind for ind, interval in enumerate(fileValues.deck)
                             if (interval.firstDate - timedelta(hours=0, minutes=moreMin) < pdDate < interval.endDate)])
    indeces2plot = indeces2plot[::3]  # skip every third element to avoid redundancy
    elements2plot = np.array(fileValues.deck)[indeces2plot]
    len_elements2plot = len(elements2plot)
    index = np.arange(0, len_elements2plot, plotsPerPage)
    pdf = matplotlib.backends.backend_pdf.PdfPages(path2plot + csv_name[:-4] + '.pdf')
    for i in index:
        tmp_el2plot = elements2plot[i:min([i + plotsPerPage, len_elements2plot])]
        fig = validation_plots(name='', df=df, plot_elements=tmp_el2plot, save=False, top=0.95)
        pdf.savefig(fig)
        plt.close()
    pdf.close()
    logger.info(f'Produced pdf with {len(index)} pages for element {csv_name[:-4]}.')


def plotAllFiles(method='', onlyRecent=False):
    signatures = load_reservoirs(method=method, onlyRecent=onlyRecent)
    for name, val in signatures.items():
        plotOneFile(fileName=name, fileValues=val)


def plotOneLong(fileName, hBefore, mBefore, hAfter, mAfter, save=False, **kwargs):
    nTicks = kwargs.get('nTicks', int(((hAfter + hBefore) * 60 + mBefore + mAfter) / 10))
    df, csv_name = open_associated_csv(pickle_file_name=fileName)
    pdDate = pd.to_datetime(datetime.strptime(csv_name.split('_')[-1].replace('.csv', '').replace('.', ':'), fmt))
    df['datetime_cf'] = [date.tz_localize(None) for date in df.datetime]
    around_pd = df.loc[(df['datetime_cf'] >= pdDate - timedelta(hours=hBefore, minutes=mBefore)) &
                       (df['datetime_cf'] <= pdDate + timedelta(hours=hAfter, minutes=mAfter))].copy()
    tmp_buy = around_pd[around_pd['side'] == 'buy'];
    tmp_sell = around_pd[around_pd['side'] == 'sell']
    fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, figsize=(14, 7.5))
    fig.suptitle(csv_name[:-4])
    locator = mdates.AutoDateLocator(minticks=nTicks, maxticks=2 * nTicks)
    formatter = mdates.ConciseDateFormatter(locator)
    formatter.zero_formats[-1] = '%M:%S';
    formatter.formats[-1] = '%H:%M:%S';
    formatter.offset_formats[-1] = '%Y-%b-%d'
    ax1.plot(tmp_buy.datetime.values, tmp_buy.price.values, 'r-', label='buy-price')
    ax1.plot(tmp_sell.datetime.values, tmp_sell.price.values, 'g-', label='sell-price')
    handles1, labels1 = ax1.get_legend_handles_labels()
    ax2.plot(tmp_buy.datetime.values, tmp_buy.volume.values, 'r--', label='buy-vol')
    ax2.plot(tmp_sell.datetime.values, tmp_sell.volume.values, 'g--', label='sell-vol')
    handles2, labels2 = ax2.get_legend_handles_labels()
    fig.legend(handles1 + handles2, labels1 + labels2, loc='lower center', fancybox=False, shadow=False, ncol=4)
    ax2.xaxis.set_major_locator(locator);
    ax2.xaxis.set_major_formatter(formatter)
    ax1.yaxis.set_minor_locator(AutoMinorLocator(4));
    ax2.xaxis.set_minor_locator(AutoMinorLocator(5))
    ax1.grid(which='major', alpha=0.6);
    ax2.grid(which='major', alpha=0.6)
    ax1.grid(which='minor', alpha=0.2);
    ax2.grid(which='minor', alpha=0.2)
    if save:
        pdf = matplotlib.backends.backend_pdf.PdfPages(path2plot + csv_name[:-4] + '_LONG.pdf')
        pdf.savefig(fig)
        pdf.close();
        plt.close()
    else:
        plt.show()


def plotAllLong(hBefore, mBefore, hAfter, mAfter, method='', onlyRecent=False):
    """
    Plot long plots around pump&dump time based on order CSV file for all signatures saved.
    @param hBefore: hours before pump
    @param mBefore: minutes before pump
    @param hAfter: hours after pump
    @param mAfter: minutes after pump
    @param randSig: want to use random signature saved files (to decide which files to load)?
    return: - (just plot_anomaly)
    """
    signatures = load_reservoirs(method=method, onlyRecent=onlyRecent)
    for name in signatures.keys():
        plotOneLong(name, hBefore, mBefore, hAfter, mAfter, save=True)


plotSubdivision = {  # (nRows, nCols)
    1: (1, 1), 2: (1, 2), 3: (1, 3), 4: (2, 2), 5: (2, 3), 6: (2, 3),
    7: (3, 3), 8: (3, 3), 9: (3, 3), 10: (3, 4), 11: (3, 4), 12: (3, 4)
}


def select_buy_sell_df(df, elements2plot, window_size=c.window_size):
    try:
        init_index = df[df['datetime'] == elements2plot.firstDate].index[0]
    except (IndexError, TypeError):
        init_index = df[df.datetime.values == elements2plot.firstDate.to_datetime64()].index[0]
    tmp_df = df.loc[init_index:init_index + window_size - 1]  # select time_windows elements
    tmp_buy = tmp_df[tmp_df['side'] == 'buy'];
    tmp_sell = tmp_df[tmp_df['side'] == 'sell']
    return tmp_df, tmp_buy, tmp_sell


@util.print_runtime
def test_plot(reservoirs, pred_dict, select_algo, select_cluster, nRows=3, nCols=4, save=True):
    """
    Show randomly selected plots and, if save is true, save them as pdf in c.img_order_folder.
    @param pred_dict: dictionary, output of clustering_algorithms (or anomaly_detection_algorithms)
    @param select_algo: string to be contained in the keys of pred_dict
    @param select_cluster: integer, cluster 0, 1 and so on
    @param nRows: number of rows for plots to show/save
    @param nCols: number of cols for plots to show/save
    @param save: save plot_anomaly?
    @return: -
    """
    nPlots = nRows * nCols
    iterable = [x for x in pred_dict.keys() if select_algo in x[0]]  # many different files, one algorithm
    for key in iterable:
        logger.info(f'Combination: {key}')
        df, csv_name = open_associated_csv(pickle_file_name=key[1])
        clusterMask = np.where(pred_dict[key] == select_cluster)[0]  # select elements of reservoirs[k] I want to take
        if clusterMask.size == 0:  # EMPTY array
            continue
        # cluster_elements = np.array([reservoirs[k[1]].deck[x] for x in clusterMask])
        cluster_elements = np.array(reservoirs[key[1]].deck)
        nPlots = min([nPlots, clusterMask.size])
        sample_elements = np.random.choice(clusterMask, nPlots, replace=False)  # sampling without replacement
        plot_elements = cluster_elements[sample_elements]  # these are the elements I want to plot_anomaly
        validation_plots(csv_name[:-4] + '_' + select_algo + '_' + str(select_cluster), df, plot_elements, save)


def validation_plots(name, df, plot_elements, save, show=False, **kwargs):
    top = kwargs.get('top', 0.96)
    nTicks = kwargs.get('n_ticks', 3)
    nRows, nCols = plotSubdivision[plot_elements.size]
    fig = plt.figure(figsize=(14, 7.5))
    outer = gsp.GridSpec(nrows=nRows, ncols=nCols, wspace=0.3, hspace=0.3, bottom=0.1, left=0.04, right=0.96, top=top)
    locator = mdates.AutoDateLocator(minticks=nTicks, maxticks=nTicks + 1)
    formatter = mdates.ConciseDateFormatter(locator)
    formatter.zero_formats[-1] = '%M:%S';
    formatter.formats[-1] = '%H:%M:%S';
    formatter.offset_formats[-1] = '%Y-%b-%d'
    for i in range(plot_elements.size):
        axes = np.empty(shape=(2, 1), dtype=object)
        local_gsp = gsp.GridSpecFromSubplotSpec(2, 1, subplot_spec=outer[i])
        tmp_df, tmp_buy, tmp_sell = select_buy_sell_df(df=df, elements2plot=plot_elements[i])
        for j in range(2):
            ax = plt.Subplot(fig, local_gsp[j, 0], sharex=axes[1, 0])
            fig.add_subplot(ax)
            ax.xaxis.set_ticks([])
            # ax.set_xticks([], minor=True); # ax.tick_params(axis='x', which='both', bottom=False)
            axes[j, 0] = ax
        for j in range(2):
            ax = axes[j, 0]
            if j == 0:
                ax.plot(tmp_buy.datetime.values, tmp_buy.price.values, 'r-', label='buy-price')
                ax.plot(tmp_sell.datetime.values, tmp_sell.price.values, 'g-', label='sell-price')
                plt.setp(ax.get_xticklabels(), visible=False)
                if i == plot_elements.size - 1:
                    handles2, labels2 = ax.get_legend_handles_labels()
            else:
                ax.plot(tmp_buy.datetime.values, tmp_buy.volume.values, 'r--', label='buy-vol')
                ax.plot(tmp_sell.datetime.values, tmp_sell.volume.values, 'g--', label='sell-vol')
                ax.xaxis.set_major_locator(locator);
                ax.xaxis.set_major_formatter(formatter)
                ax.xaxis.set_ticks([tmp_df.datetime.values[0], tmp_df.datetime.values[-1]])
    handles, labels = ax.get_legend_handles_labels()
    fig.legend(handles2 + handles, labels2 + labels, loc='lower center', fancybox=False, shadow=False, ncol=4)
    if save:
        plt.savefig(os.getcwd() + c.img_order_folder + name + '.pdf')
        plt.close()
    if show:
        plt.show()
    return fig


def get_full_pr_plot(precision_dict, recall_dict, precision_dict_bench, recall_dict_bench, suffix=''):
    """

    :param precision_dict: dictionary of true positive rates
    :param recall_dict:dictionary of false negative rates
    :param precision_dict_bench: dictionary of true positive rates
    :param recall_dict_bench: dictionary of false negative rates
    :param suffix: suffix for files
    :return:
    """
    fig, ax = plt.subplots(1, max(len(precision_dict), 2), sharex=True, sharey=True, figsize=(14, 7.5))

    for ind, key in enumerate(precision_dict):
        sns.lineplot(x=recall_dict[key], y=np.array(precision_dict[key]), label='Reservoir computing',
                     drawstyle='steps-pre',
                     ax=ax[ind], color='tab:blue', ci=None)
        sns.lineplot(x=np.array(recall_dict_bench[key]), y=precision_dict_bench[key],
                     label='Benchmark', drawstyle='steps-pre',
                     ax=ax[ind], color='tab:grey', ci=None)

        f1_score = [2 / (recall_dict[key][ind] ** (-1) + precision_dict[key][ind] ** (-1))
                    if recall_dict[key][ind] != 0 else 0.0 for ind in range(len(recall_dict[key]))]
        f1_score_bench = [2 / (recall_dict_bench[key][ind] ** (-1) + precision_dict_bench[key][ind] ** (-1))
                          if recall_dict_bench[key][ind] != 0 else 0.0 for ind in range(len(recall_dict_bench[key]))]

        ax[ind].vlines(recall_dict[key][np.argmax(f1_score)], 0, 1, linestyles='dashed',
                       label=f'Threshold RC F1:{round(np.max(f1_score), 2)}', color='tab:blue')
        ax[ind].vlines(recall_dict_bench[key][np.argmax(f1_score_bench)], 0, 1, linestyles='dashed',
                       label=f'Threshold Benchmark F1:{round(np.max(f1_score_bench), 2)}', color='tab:grey')

        ax[ind].set_xlabel('Recall')
        ax[ind].set_ylabel('Precision')
        ax[ind].title.set_text(f'Algorithm: {key}')
        ax[ind].legend()
        ax[ind].set_xlim([0.5, 1])
    fig.savefig('PLOTS/' + f'precision_recall_{suffix}.png', bbox_inches='tight', dpi=300)
    plt.close()


# adds a rolling average column with specified window size to a given df and col
def add_RA(df, win_size, col, name):
    df[name] = pd.Series.rolling(df[col], window=win_size, center=False).mean()


# finds volume spikes in a given df, with a certain threshold and window size
# returns a (boolean_mask,dataframe) tuple
def find_vol_spikes(df, v_thresh, win_size):
    # -- add rolling average column to df --
    vRA = str(win_size) + 'h Volume RA'
    add_RA(df, win_size, 'volume', vRA)
    # -- find spikes --
    vol_threshold = v_thresh * df[vRA]  # v_thresh increase in volume
    vol_spike_mask = df["volume"] > vol_threshold  # where the volume is at least v_thresh greater than the x-hr RA
    df_vol_spike = df[vol_spike_mask]
    return vol_spike_mask, df_vol_spike


# finds price spikes in a given df, with a certain threshold and window size
# returns a (boolean_mask,dataframe) tuple
def find_price_spikes(df, p_thresh, win_size):
    # -- add rolling average column to df --
    pRA = str(win_size) + 'h Close Price RA'
    add_RA(df, win_size, 'price', pRA)
    # -- find spikes --
    p_threshold = p_thresh * df[pRA]  # p_thresh increase in price
    p_spike_mask = df["price"] > p_threshold  # where the high is at least p_thresh greater than the x-hr RA
    df_price_spike = df[p_spike_mask]
    return p_spike_mask, df_price_spike


def find_price_dumps(df, win_size):
    pRA = str(win_size) + "h Close Price RA"
    pRA_plus = pRA + "+" + str(win_size)
    df[pRA_plus] = df[pRA].shift(-win_size)
    price_dump_mask = df[pRA_plus] <= (df[pRA] + df[pRA].std())
    # if the xhour RA from after the pump was detected is <= the xhour RA (+std dev) from before the pump was detected
    # if the price goes from the high to within a range of what it was before
    df_p_dumps = df[price_dump_mask]
    return price_dump_mask, df_p_dumps


def select_time_df_index(df, days_before_after):
    ## Restrict df based on index of type datetime
    assert days_before_after <= 14, 'days_before_after has to be <= 7'
    midpoint = df.index[round(len(df) / 2)]
    return df[(df.index <= midpoint + timedelta(days=days_before_after)) &
              (df.index >= midpoint - timedelta(days=days_before_after))]


@util.print_runtime
def get_pr(out_path, suffix='', rounding_size=1, get_benchmark=True, days_before_after=None, data_folder='data/'):
    """
    @param out_path: output path of
    @param suffix: suffix for file names
    @param rounding_size: spilt data in rounding_size chunks
    @param get_benchmark: flag to check if benchmark should be calculated
    @param days_before_after: how many days around each pump and dump case to include
    @param data_folder: folder where csv files are stored
    @return:
    """
    prediction_results = pd.read_csv(out_path)
    prediction_results = prediction_results.set_index(prediction_results.columns[0])
    relevant_cols = prediction_results.loc[:, ['Method', 'Symbol', 'Is_Pump_Dump', 'Prediction',
                                               'Filename', 'Window_end', 'Window_start']]
    relevant_cols = relevant_cols.set_index('Method')
    pr_dict = {}
    recall_dict = {}
    if get_benchmark:
        pr_dict_bench = {}
        recall_dict_bench = {}
    if c.run_anomaly_prediction_plots:
        logger.info('Generating anomaly plots')
    for method in relevant_cols.index.unique():
        logger.info(f'Processing results of algorithm {method}')
        all_benchmark = []
        all_predictions = []
        all_labels = []
        for _, file_name in enumerate(relevant_cols['Filename'].unique()):
            if _ % 10 == 0:
                logger.debug(f'Symbol number {_}')
            predictions = relevant_cols.loc[relevant_cols['Filename'] == file_name, 'Prediction'].loc[method]
            predictions = np.convolve(predictions, np.ones(c.pred_conv_kernel) / c.pred_conv_kernel,
                                      mode='same')  # efficient way of computing moving averages
            labels = relevant_cols.loc[relevant_cols['Filename'] == file_name, 'Is_Pump_Dump'].loc[method]
            order_data = pd.read_csv(data_folder + file_name.replace('_RAND.pl', '.csv').replace('_BENCH.pl', '.csv').
                                     replace('.pl', '.csv'))
            order_data['datetime'] = [datetime.fromisoformat(date.replace('Z', '').replace('T', ' '))
                                      for date in order_data.datetime]
            filtered_data = relevant_cols.loc[relevant_cols['Filename'] == file_name].loc[method]
            filtered_data['Window_end'] = filtered_data['Window_end'].apply(lambda x: datetime.fromisoformat(x))
            filtered_data['Labels'] = labels
            filtered_data['Predictions'] = -predictions

            merged = filtered_data.merge(order_data, how='left', left_on='Window_end',
                                         right_on='datetime')
            merged = merged.dropna()
            merged['DatetimeMinute'] = merged['datetime'].apply(lambda x: datetime.fromtimestamp(
                round(x.timestamp() / (60 * rounding_size)) * 60 * rounding_size))

            df_averaged = merged.groupby('DatetimeMinute').agg({'Labels': 'max', 'Predictions': 'sum',
                                                                'price': 'mean', 'volume': 'sum'
                                                                })  # DatetimeMinute becomes new index
            # from article https://crimesciencejournal.biomedcentral.com/articles/10.1186/s40163-018-0093-5
            # best config used in article: 12 hours, 1.05 price spike, 3 vol spike. in order to get the full range of
            pmask, pdf = find_price_spikes(df_averaged, 1.05, 12)
            vmask, vdf = find_vol_spikes(df_averaged, 3, 12)
            final_combined_mask = vmask & pmask
            df_averaged['Benchmark_prediction'] = final_combined_mask
            df_averaged['Predictions_normalised'] = df_averaged['Predictions'] / \
                                                    np.max(np.abs(df_averaged['Predictions']))
            df_averaged['Price_spikes'] = pmask
            df_averaged['Volume_spikes'] = vmask
            if days_before_after is not None:
                df_averaged = select_time_df_index(df_averaged, days_before_after)
                # midpoint = df_averaged.index[round(len(df_averaged) / 2)]
                # df_averaged = df_averaged[(df_averaged.index <= midpoint + timedelta(days=days_before_after)) &
                #                           (df_averaged.index >= midpoint - timedelta(days=days_before_after))]
            all_predictions += list(df_averaged['Predictions_normalised'])
            all_labels += list(df_averaged['Labels'])

            if c.run_anomaly_prediction_plots:
                df_averaged['price'] = df_averaged['price'] / max(df_averaged['price']) * 100
                plt.figure(figsize=(10, 5))
                ax = sns.lineplot(x=df_averaged.index, y=df_averaged['price'], color='black', label="Price")
                ax.set(xlabel='Date', ylabel='Normalised price')
                markers_prediction = np.argwhere(np.array(df_averaged['Predictions_normalised']) > 0.7).flatten()
                markers_bench = np.argwhere(np.array(df_averaged['Benchmark_prediction']) == True).flatten()
                markers_price_spikes = np.argwhere(np.array(df_averaged['Price_spikes']) == True).flatten()
                markers_vol_spikes = np.argwhere(np.array(df_averaged['Volume_spikes']) == True).flatten()

                sns.scatterplot(x=df_averaged.index[markers_bench], y=df_averaged['price'][markers_bench] * 1.005,
                                marker='o', label='Benchmark prediction', color='g', s=100)
                sns.scatterplot(x=df_averaged.index[markers_prediction],
                                y=df_averaged['price'][markers_prediction],
                                marker="d", label='Reservoir prediction', color='y', s=120)
                sns.scatterplot(x=df_averaged.index[markers_price_spikes],
                                y=df_averaged['price'][markers_price_spikes],
                                marker='*', label='Price spikes', color='m', s=80)

                sns.scatterplot(x=df_averaged.index[markers_vol_spikes],
                                y=df_averaged['price'][markers_vol_spikes],
                                marker='x', label='Volume spikes', color='r', s=80)

                plt.fill_between(df_averaged.index, plt.ylim()[0], plt.ylim()[1], color='b', alpha=0.3,
                                 where=df_averaged['Labels'] == True, label='Pump and Dump case')
                plt.legend()
                plt.setp(ax.get_xticklabels(), rotation=30, horizontalalignment='right')

                plt.savefig(c.plot_anomaly_scores + f'{method}_{file_name}_{suffix}.png', dpi=300,
                            bbox_inches='tight')
                plt.close()

            # linearly interpolate between 1 *  vol and 3 * vol and 1 * price and 1.05 * price
            steps = 100
            price_vol_interpolation = (np.linspace(0, 1.05, steps), np.linspace(0, 3, steps))
            if get_benchmark:
                final_combined_masks = []
                for i in range(steps):
                    price_spike = price_vol_interpolation[0][i]
                    vol_spike = price_vol_interpolation[1][i]
                    pmask, pdf = find_price_spikes(df_averaged, price_spike, 12)
                    vmask, vdf = find_vol_spikes(df_averaged, vol_spike, 12)
                    final_combined_masks.append((vmask & pmask) * i * 0.01)
                df_averaged['Benchmark_prediction'] = np.max(final_combined_masks, axis=0)
            if get_benchmark:
                all_benchmark += list(df_averaged['Benchmark_prediction'])
        all_labels = np.array(all_labels)
        true_labels = np.where(all_labels == True)[0]
        case_dict = {}
        case_num = 0
        label_cache = 0
        for label in true_labels:
            if label != label_cache + 1:
                case_num += 1
            if case_num in case_dict:
                case_dict[case_num].append(label)
            else:
                case_dict[case_num] = [label]

            label_cache = label

        all_predictions = np.array(all_predictions) / np.max(np.abs(all_predictions))
        pr_dict[method] = [util.get_precision(all_predictions, case_dict, cutoff)
                           for cutoff in np.linspace(min(all_predictions) - 0.1,
                                                     max(all_predictions) + 0.1, 300)]
        recall_dict[method] = [util.get_recall(all_predictions, case_dict, cutoff)
                               for cutoff in np.linspace(min(all_predictions) - 0.1,
                                                         max(all_predictions) + 0.1, 300)]
        if get_benchmark:
            all_benchmark = np.array(all_benchmark) / np.max(np.abs(all_benchmark))
            pr_dict_bench[method] = [util.get_precision(all_benchmark, case_dict, cutoff)
                                     for cutoff in np.linspace(min(all_benchmark) - 0.1,
                                                               max(all_benchmark) + 0.1, 300)]
            recall_dict_bench[method] = [util.get_recall(all_benchmark, case_dict, cutoff)
                                         for cutoff in np.linspace(min(all_benchmark) - 0.1,
                                                                   max(all_benchmark) + 0.1, 300)]
    if get_benchmark:
        return pr_dict, recall_dict, pr_dict_bench, recall_dict_bench
    else:
        return pr_dict, recall_dict


def main_plot2StudyPD(method='', onlyRecent=False):
    # plot_flagged_interval_as_PD()
    plotAllFiles(method=method, onlyRecent=onlyRecent)
    plotAllLong(hBefore=0, mBefore=31, hAfter=1, mAfter=11, method=method, onlyRecent=onlyRecent)


def main_plotAlgorithms():
    useRandSig = True
    if 'Matteo' in platform.uname()[1]:
        useRandSig = False
    if c.run_cluster_plots:
        if useRandSig:
            method = ''
        else:
            method = 'random'
        clusterPred = pickle.load(open('final_outputs/predictions.pl', 'rb'))
        reservoirs = load_reservoirs(method=method)
        for algo in c.clustAlgoSTR:
            test_plot(pred_dict=clusterPred, reservoirs=reservoirs, select_algo=algo, select_cluster=0)
            test_plot(pred_dict=clusterPred, reservoirs=reservoirs, select_algo=algo, select_cluster=1)
            test_plot(pred_dict=clusterPred, reservoirs=reservoirs, select_algo=algo, select_cluster=2)
            test_plot(pred_dict=clusterPred, reservoirs=reservoirs, select_algo=algo, select_cluster=3)
    if useRandSig:
        precision_dict_rand, recall_dict_rand = \
            get_pr(os.path.join(c.prediction_output, 'random_reservoirs_predictions.csv'), 'rand', rounding_size=60,
                   get_benchmark=False, days_before_after=7)
    precision_dict_real, recall_dict_real, precision_dict_bench, recall_dict_bench = \
        get_pr(os.path.join(c.prediction_output, 'reservoirs_predictions.csv'),
               'real', rounding_size=60, days_before_after=7)
    get_full_pr_plot(precision_dict_real, recall_dict_real, precision_dict_bench, recall_dict_bench, 'real')

    if useRandSig:
        precision_dict_rand, recall_dict_rand = \
            get_pr(os.path.join(c.prediction_output, 'random_reservoirs_predictions.csv'), 'rand', rounding_size=60,
                   get_benchmark=False, days_before_after=7)
        get_full_pr_plot(precision_dict_rand, recall_dict_rand, precision_dict_bench, recall_dict_bench, 'rand')


if __name__ == '__main__':
    # main_plot2StudyPD(onlyRecent=True)
    main_plotAlgorithms()
