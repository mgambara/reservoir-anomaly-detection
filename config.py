#### reservoirs parameters
sig_degree = 3
rand_res_dim = 50
variance = 0.1
mean = 0.05

#### general path parameters
window_size = 100
cutoff = 5

#### paths
sigmat_folder = '/sigMat/'
raw_data_folder = '/data/'
img_order_folder = '/img/order/'
plot_flags_pd = '/PLOTS/plot_flag_pd/'
plot_anomaly_scores = 'PLOTS/plot_anomaly/'
prediction_output = 'final_outputs/'

#### algorithm
clustAlgoSTR = ['KMeans', 'SpectralClustering', 'GaussianMixture', 'DBSCAN']
cluster_number = 2  # just 2 because of anomaly detection purpose
startAlgo_number = 10  # how many times I want to restart the algorithm (for clustering)

train_symbols_separate = False
pred_conv_kernel = 1
use_buy_sell_flag = False

#### run parameters
run_cluster_plots = False
run_anomaly_prediction_plots = False

