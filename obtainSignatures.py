import os
import util
import numpy as np
import pandas as pd
import config as c
import RC

from datetime import datetime
from datetime import timedelta
from sklearn.preprocessing import MinMaxScaler
from iisignature import sig
from signature_class import oneSignature, sigDeck
from pull_crypto_data_no_ohlcv import get_pd_attempts
import pickle

pd.set_option('mode.chained_assignment', None)
logger = util.get_logger(__name__)

fmt = "%Y-%m-%d %H:%M"
fmtPlus = "%Y-%m-%d %H:%M:%S.%f"
csv_url = 'https://raw.githubusercontent.com/SystemsLab-Sapienza/pump-and-dump-dataset/master/pump_telegram.csv'
# pd_attempts = pd.read_csv(csv_url)
folder2data = '/data/'
# path2files = os.getcwd()+folder2data+'binance/'
path2files = os.getcwd() + folder2data
hp_rc = {'res_size': c.rand_res_dim, 'varA': np.sqrt(c.variance),
         'mean': c.mean}  # hyper-parameters reservoir computing


model_res = RC.RCClassification(dim_input=4, dim_output=4, hparams=hp_rc,
                                sigmoid=lambda x: np.tanh(x))
model_res.generate_reservoir()


def relu(x):
    return np.maximum(x, 0)


def df_print(df):
    with pd.option_context('display.max_columns', None):
        print(df)


def aggregate_orders_over_time(df):
    """
    Aggregate over all columns except for volume and amount
    @param df: dataframe over which we aggregate
    @return: dataframe with aggregate fields (volume and amount)
    """
    return df.groupby(['symbol', 'timestamp', 'datetime', 'side', 'price'])[
        ['amount', 'volume']].sum().reset_index().copy()


def pumpTime(df):
    if 'date' not in df.columns or 'hour' not in df.columns:
        logger.error(f'Columns date or hour missing in pumpTime! COLUMNS: {df.columns}')
    df['dateTime'] = df['date'] + ' ' + df['hour']
    # df['pumpTime'] = [datetime.strptime(date, fmt).strftime(fmt) for date in df['dateTime']]
    return df


def select_files(symbol, df, exchange='binance', path=path2files):
    dates = pd.unique(df[(df['symbol'] == symbol) & (df['exchange'] == exchange)].dateTime.values)
    order_btc = [];
    ohlcv_btc = []
    order_eth = [];
    ohlcv_eth = []
    for d in dates:
        order_btc.append(path + 'orders_{}_btc_{}'.format(symbol, str(d).replace(':', '.') + '.csv'))
        ohlcv_btc.append(path + 'ohlcv_{}_btc_{}'.format(symbol, str(d).replace(':', '.') + '.csv'))
        order_eth.append(path + 'orders_{}_eth_{}'.format(symbol, str(d).replace(':', '.') + '.csv'))
        ohlcv_eth.append(path + 'ohlcv_{}_eth_{}'.format(symbol, str(d).replace(':', '.') + '.csv'))
    return order_btc, ohlcv_btc, order_eth, ohlcv_eth


def select_file(symbol, dt, path=path2files):
    order_btc = path + 'orders_{}_btc_{}'.format(symbol, str(dt).replace(':', '.') + '.csv')
    order_eth = path + 'orders_{}_eth_{}'.format(symbol, str(dt).replace(':', '.') + '.csv')
    return order_btc, order_eth  # , ohlcv_btc, ohlcv_eth


def checkNames(df, colNames, funcName):
    if any([item not in df.columns for item in colNames]):
        logger.error(f'colNames has some names which are not in df.columns!')
        logger.error(f'COLUMNS: {df.columns}')
        logger.error(f'FUNCTION NAME: {funcName}')


def process_df(df, colNames, returns=False):
    colNames = colNames.copy()
    if 'amount' in df.columns:
        del df['amount']
    if 'type' in df.columns:
        del df['type']  # since it is empty when using Binance
    if returns:
        df.loc[:, 'return_prices'] = df['price'] / df['price'].shift(1) - 1.
        df = df.iloc[1:]  # first row of 'return_prices' is NaN now --> skip
        colNames.remove('price')
        colNames.append('return_prices')
        df.index = range(len(df))
    checkNames(df, colNames + ['side'], 'process_df')
    df.loc[:, 'side'] = [0.5 if s == 'buy' else -0.5 for s in df['side'].copy()]  # Try without sides
    # return df, ''
    scaler = MinMaxScaler()
    df.loc[:, colNames] = scaler.fit_transform(df[colNames])
    return df, scaler


def getPDAttemptsTimes():
    """
    @return: data frame with P&D estimated duration attempts.
    """
    dft = pd.read_csv('PumpDumpTime.csv', usecols=[0, 1, 2, 3, 4, 5], sep=';')
    dft = dft.dropna()  # drop rows with at least 1 NA
    return dft


def createSigList(TS, inpDim, starters, dates, mins, secs, pdDate, method='', extendFlag=False):
    sigList = []
    if method == 'benchmark':
        _maxrange = TS['timestamp'].iloc[-1] - TS['timestamp'].iloc[0]
        lower_lim = TS['timestamp'].iloc[0]
        TS = TS[['timestamp', 'price', 'volume']]
        for i in range(len(starters)):
            upper_lim = lower_lim + _maxrange / len(starters)
            window = TS[(lower_lim <= TS['timestamp']) & (TS['timestamp'] < upper_lim)]
            if len(window) == 0:
                lower_lim = upper_lim
                continue
            startInd = window.index[0]
            endInd = window.index[-1]
            startDate = dates[startInd];
            endDate = dates[endInd]
            tmpStartDate = startDate - timedelta(minutes=mins, seconds=secs) if extendFlag else startDate
            includePD = True if tmpStartDate < pdDate < endDate < startDate + timedelta(minutes=mins, seconds=secs) \
                                or startDate < pdDate < endDate else False
            window = window[['price', 'volume']]  # benchmark is J. Kamps and B. Kleinberg, 2018
            sigVec = np.array(window.mean(0))
            sigList.append(oneSignature(firstDate=startDate, endDate=endDate, sig=sigVec, includePD=includePD))
            lower_lim = upper_lim
    else:
        TS = np.array(TS)
        for i in range(len(starters)):
            startInd = starters[i];
            endInd = starters[i] + c.window_size
            timeWindow = np.arange(startInd, endInd);
            startDate = dates[startInd];
            endDate = dates[endInd - 1]
            tmpStartDate = startDate - timedelta(minutes=mins, seconds=secs) if extendFlag else startDate
            # There are 2 ways of getting more P&D attempt than the default way (startDate < pdDate < endDate)
            # 1st: gets all intervals which intersects the interval which was classified as PD attempt
            # includePD = True if tmpStartDate < pdDate < endDate else False
            # 2nd: gets all intervals which intersects the interval which was classified as PD attempt AND which
            # terminates inside the interval which was classified as PD attempt (so we have fewer intervals wrt 1st way)
            includePD = True if tmpStartDate < pdDate < endDate < startDate + timedelta(minutes=mins, seconds=secs) \
                                or startDate < pdDate < endDate else False
            # if includePD:
            #     print(i); print(startDate); print(endDate)  # USEFUL for debugging
            if method == 'random':
                # model_res = RC.py.RCClassification(dim_input=inpDim, dim_output=inpDim, hparams=hp_rc,
                #                                 sigmoid=lambda x: np.tanh(x))
                # model_res.generate_reservoir()
                # TS[timeWindow][:, 3] /= 10  # side
                sigVec = model_res.reservoir(TS[timeWindow])
            else:
                # sigMat[i] = sig(TS[timeWindow], sigDegree)
                sigVec = sig(TS[timeWindow], c.sig_degree)  # standard signature
            sigList.append(oneSignature(firstDate=startDate, endDate=endDate, sig=sigVec, includePD=includePD))
    return sigList


def symbolSignatures(TS, dates, pdDate, method='', extendFlag=False, mins=5, secs=0):
    nRows = TS.shape[0] - TS.shape[0] % c.cutoff
    nCols = TS.shape[1]
    starters = np.arange(0, nRows - c.window_size + c.cutoff, c.cutoff)
    sigList = createSigList(TS, nCols, starters, dates, mins, secs, pdDate, method, extendFlag)
    return sigList


@util.print_runtime
def loop_Signature(df, save=True, returns=False, force_overwrite=False, method='', extendFlag=False, DURATION=False):
    """
    Loop over dataframe df containing all PD attempts per coin and time to obtain signatures of each trade-window.
    @param df: PD attempts dataframe
    @param save: save signatures?
    @param returns: use returns of prices instead of prices themselves?
    @param force_overwrite: overwrite current signatures?
    @param method: standard, random or benchmark
    @param extendFlag: extend PD duration in an automatic way?
    @param DURATION: use PD list with manually set PD duration (more precise)?
    @return: - (if save is True, then save the computed signatures)
    """
    colNames = ['timestamp', 'price', 'volume']
    if DURATION:  # replace df with another which has P&D duration
        extendFlag = True  # FORCE TO TRUE
        df = pumpTime(getPDAttemptsTimes())
    else:
        df = pumpTime(df.copy())
    for t in df.itertuples():  # t[1] is symbol, t[7] is dateTime
        paths2data = select_file(symbol=t[1], dt=t[7])
        file = paths2data[0]  # select ONLY combination order - BTC for the moment
        str_fileName = file.replace(folder2data, c.sigmat_folder).replace('.csv', '.pl')
        if method == 'random':
            str_fileName = str_fileName.replace('.pl', '_RAND.pl')
        elif method == 'benchmark':
            str_fileName = str_fileName.replace('.pl', '_BENCH.pl')
        if os.path.exists(str_fileName) and not force_overwrite:
            logger.info(f'Reservoirs already exist for {t[1]}')
            continue
        if os.path.exists(file) and os.path.getsize(file) > 1:  # os.path.getsize(file) > 1 avoids empty files
            logger.info(f'Generating reservoirs for {t[1]} method: {method}')
            pdDateTime = datetime.strptime(file.split('_')[-1].replace('.csv', '').replace('.', ':'), fmt)
            if DURATION:  # t[5] is minutes and t[6] is seconds (in this case)
                min = t[5];
                sec = t[6] + 1
            elif extendFlag and not DURATION:
                min = 5;
                sec = 0  # 5 minutes by default if we want to extend the signature interval
            else:
                min = 0;
                sec = 0
            symbol_btc_orders = aggregate_orders_over_time(pd.read_csv(file))
            symbol_btc_orders, _ = process_df(df=symbol_btc_orders, colNames=colNames, returns=returns)
            symbol_btc_orders['datetime'] = [datetime.strptime(d.replace('T', ' ').replace('Z', ''), fmtPlus) for d in
                                             symbol_btc_orders['datetime']]
            checkNames(symbol_btc_orders, colNames, 'loop_Signature')
            ts = symbol_btc_orders[colNames + ['side']]
            # ts = np.array(symbol_btc_orders[colNames])  # without 'side'
            sigList = symbolSignatures(ts, symbol_btc_orders['datetime'], pdDateTime, method, extendFlag,
                                       mins=min, secs=sec)
            if save:
                with open(str_fileName, 'wb') as handle:
                    pickle.dump(sigDeck(sigList), handle)


def run():
    pd_attempts = get_pd_attempts(remove=True)
    # reservoirs using random signatures
    loop_Signature(df=pd_attempts.loc[pd_attempts['exchange'] == 'binance'], method='random',
                   returns=True, force_overwrite=True, DURATION=True)

    # reservoirs using real signatures
    loop_Signature(df=pd_attempts.loc[pd_attempts['exchange'] == 'binance'],
                   force_overwrite=True, returns=True, extendFlag=True, DURATION=True)


if __name__ == '__main__':
    run()
