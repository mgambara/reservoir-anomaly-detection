import abc
import numpy as np

import iisignature
from sklearn.linear_model import LogisticRegression, Ridge


class AbstractReservoir(abc.ABC):

    @abc.abstractmethod
    def predict(self, test_data, dt):
        pass

    @abc.abstractmethod
    def fit(self, x_train, y_train, dt, hparams, method=None, *args):
        pass

    @abc.abstractmethod
    def get_params(self):
        pass

    @abc.abstractmethod
    def generate_reservoir(self, *args):
        pass


class RCClassification(AbstractReservoir):
    def __init__(self, dim_input, dim_output, hparams, sigmoid=lambda x: x):
        self.hparams = self._build_hparams(hparams)
        self.randomProjection = None
        self.dimInput = dim_input
        self.dimOutput = dim_output
        if self.hparams['method'] == 'logistic':
            self.readout = LogisticRegression(solver='lbfgs')
        else:
            self.readout = Ridge(0.001)
        self.nStates = None
        self.randomBias = None
        self.sigmoid = sigmoid

    def _build_hparams(self, hparams):
        """

        :param hparams: dictionary of hyper parameters
        :return: full hyper parameter required for method, with all default values
        """
        mod_hparams = {}
        self._fill_default('varA', 0.3, hparams, mod_hparams)
        self._fill_default('mean', 0, hparams, mod_hparams)
        self._fill_default('res_size', 50, hparams, mod_hparams)
        self._fill_default('method', 'ridge', hparams, mod_hparams)
        self._fill_default('window_size', 60, hparams, mod_hparams)
        self._fill_default('cutoff', 30, hparams, mod_hparams)
        self._fill_default('real_signature', False, hparams, mod_hparams)

        return mod_hparams

    @staticmethod
    def _fill_default(key, value, dict_source, dict_target):
        if key in dict_source:
            dict_target[key] = dict_source[key]
        else:
            dict_target[key] = value

    def _get_random_projections(self, d):
        random_projection = []
        random_bias = []
        for i in range(d):
            projection = np.random.normal(self.hparams['mean'], self.hparams['varA'],
                                          (self.hparams['res_size'], self.hparams['res_size']))
            norm = np.linalg.norm(projection, 2)
            projection = projection / norm * 0.99
            random_projection.append(projection)
            random_bias.append(np.random.normal(self.hparams['mean'], self.hparams['varA'], size=self.hparams['res_size']))

        return np.array(random_projection), np.array(random_bias)

    def _reservoir_field(self, state, increment):
        return self.sigmoid(np.dot(self.randomProjection, state) + self.randomBias).T.dot(increment)

    def reservoir(self, observation, real_sig=False):
        if real_sig:
            level = np.ceil(np.log((self.hparams['res_size'] + 1) * (self.dimInput - 1) + 1) / np.log(self.dimInput) - 1)
            R = iisignature.sig(observation, int(level), 0)
        else:
            R = np.ones(len(self.randomProjection[0]))
            dY = np.diff(observation, axis=0)
            for i in range(len(dY)):
                increment = self._reservoir_field(R, dY[i])
                try:
                    R += increment
                except ValueError:
                    R += increment.flatten()
        return R

    def generate_reservoir(self, force_overwrite=False):
        if self.randomProjection is None or force_overwrite:
            self.randomProjection, self.randomBias = self._get_random_projections(self.dimInput)
        self.nStates = self.randomProjection.shape[1]

    def _get_window_reservoirs(self, x, real_sig=False):
        reservoirs = []
        for path in x:
            res = []
            for i in range(0, len(path) - self.hparams['window_size'], self.hparams['cutoff']):
                res.append(self.reservoir(path[i:i + self.hparams['window_size']], real_sig))
            reservoirs.append(np.array(res))
        return np.array(reservoirs)

    def fit(self, x_train, y_train, method=None, **kwargs):
        """
        :param x_train: training features
        :param y_train: labeled target
        :param method: classification method
        :param kwargs: arguments used by various regression / machine learning methods
        :return: naive regression using states as feature set
        """
        if self.randomProjection is None:
            self.generate_reservoir()

        try:
            len(x_train)
        except TypeError:
            x_train = [x_train]

        method = self.hparams['method']
        if method == 'ridge':
            _states = [self.reservoir(x_train[i], self.hparams['real_signature']) for i in range(len(x_train))]
            self.readout.fit(_states, y_train, sample_weight=kwargs.get('sample_weight', None))
            return np.array(_states)
        elif method == 'logistic':
            _states = [self.reservoir(x_train[i], self.hparams['real_signature']) for i in range(len(x_train))]
            self.readout.fit(_states, y_train)
            return _states
        elif method == 'window':
            self.readout = []
            reservoirs = self._get_window_reservoirs(x_train, self.hparams['real_signature']) # dimension: (sample size, number windows, reservoir size)
            for window in range(reservoirs.shape[1]):
                model_lm = Ridge(0.001)
                res = reservoirs[:, window, :]
                model_lm.fit(res, y_train, sample_weight=kwargs.get('sample_weight', None))
                self.readout.append(model_lm)
            return reservoirs
        elif method == 'stackedwindow':
            reservoirs = self._get_window_reservoirs(x_train, self.hparams['real_signature']) # dimension: (sample size, number windows, reservoir size)
            reservoirs = reservoirs.reshape(reservoirs.shape[0], reservoirs.shape[1] * reservoirs.shape[2])
            self.readout.fit(reservoirs, y_train, sample_weight=kwargs.get('sample_weight', None))
            return reservoirs

    def round(self, predictions):
        '''

        :param predictions: returns from the predict proba function
        :return:
        '''
        _rounded = predictions.copy()
        _rounded[predictions < 0.5] = 0
        _rounded[predictions >= 0.5] = 1
        return _rounded

    def predict(self, test_data, **kwargs):
        method = self.hparams['method']
        if method == 'ridge':
            _states = [self.reservoir(test_data[i], self.hparams['real_signature']) for i in range(len(test_data))]
            return self.round(self.readout.predict(_states))
        elif method == 'logistic':
            _states = [self.reservoir(test_data[i], self.hparams['real_signature']) for i in range(len(test_data))]
            return self.readout.predict(_states)
        elif method == 'window':
            reservoirs = self._get_window_reservoirs(test_data, self.hparams['real_signature'])  # (sample size, number windows, reservoir size)
            predictions = []
            for num, window in enumerate(range(reservoirs.shape[1])):
                predictions.append(self.readout[num].predict(reservoirs[:, window, :]))
            return self.round(np.mean(predictions, axis=0))
        elif method == 'stackedwindow':
            reservoirs = self._get_window_reservoirs(test_data, self.hparams['real_signature'])  # (sample size, number windows, reservoir size)
            reservoirs = reservoirs.reshape(reservoirs.shape[0], reservoirs.shape[1] * reservoirs.shape[2])
            return self.round(self.readout.predict(reservoirs))

    def predict_proba(self, test_data):
        method = self.hparams['method']
        if method == 'ridge':
            _states = [self.reservoir(test_data[i], self.hparams['real_signature']) for i in range(len(test_data))]
            return self.readout.predict(_states)
        elif method == 'logistic':
            _states = [self.reservoir(test_data[i], self.hparams['real_signature']) for i in range(len(test_data))]
            return self.readout.predict_proba(_states)
        elif method == 'window':
            reservoirs = self._get_window_reservoirs(test_data, self.hparams['real_signature'])  # (sample size, number windows, reservoir size)
            predictions = []
            for num, window in enumerate(range(reservoirs.shape[1])):
                predictions.append(self.readout[num].predict(reservoirs[:, window, :]))
            return np.array(predictions)

        elif method == 'stackedwindow':
            reservoirs = self._get_window_reservoirs(test_data, self.hparams['real_signature'])  # (sample size, number windows, reservoir size)
            reservoirs = reservoirs.reshape(reservoirs.shape[0], reservoirs.shape[1] * reservoirs.shape[2])
            return self.readout.predict(reservoirs)

    def get_params(self):
        return self.hparams

    def num_params(self):
        if self.hparams['method'] == 'window':
            return sum([len(model.coef_) for model in self.readout])
        else:
            return len(self.readout.coef_)

    def score(self, x, y):
        return sum(self.predict(x) == y) / len(y)