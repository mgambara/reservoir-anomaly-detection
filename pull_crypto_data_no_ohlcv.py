import ccxt
from ccxt.base.errors import RequestTimeout
import pandas as pd
from datetime import datetime
from datetime import timedelta
import time
import util
import os

logger = util.get_logger(__name__)

candles_sticks = {'tiny': {'str': '1m', 'num': 1},
                  'small': {'str': '5m', 'num': 5},
                  'medium': {'str': '15m', 'num': 15},
                  'large': {'str': '30m', 'num': 30},
                  'big': {'str': '1h', 'num': 60},
                  'bigger': {'str': '2h', 'num': 120}
                  }
params = {  # for ccxt-exchange
    'enableRateLimit': True
}
## Change to have different exchanges: "binance", "yobit", "kucoin", "bittrex", "cryptopia".
exchange_str = 'binance'
ccxt_exchange = getattr(ccxt, exchange_str)(params)
ccxt_exchange.load_markets()
# since = 1545411600000  # check: datetime.fromtimestamp(since//1000)
# binance.fetch_ohlcv('XRP/BTC', since=since, timeframe='1m')
ten_minutes = 60000 * 10  # ten minutes in milliseconds
csv_url = 'https://raw.githubusercontent.com/SystemsLab-Sapienza/pump-and-dump-dataset/master/pump_telegram.csv'


def get_pd_attempts(remove=False, **kwargs):
    csv_url = kwargs.get('csv_url',
                         'https://raw.githubusercontent.com/SystemsLab-Sapienza/pump-and-dump-dataset/master/pump_telegram.csv')
    df = pd.read_csv(csv_url)
    if remove:
        ids = []
        ## Different groups and different times (1, 2 minutes difference)
        ids.append(df[(df['symbol'] == 'YOYOW') & (df['hour'] == '18:02')].index.values[0])
        ids.append(df[(df['symbol'] == 'MDA') & (df['hour'] == '17:00')].index.values[0])
        ids.append(df[(df['symbol'] == 'GVT') & (df['hour'] == '18:00')].index.values[0])
        ## Different groups, but same time
        ids.append(df[(df['symbol'] == 'NAV') & (df['group'] == 'MPG') & (df['hour'] == '19:00')].index.values[0])
        ids.append(df[(df['symbol'] == 'BNT') & (df['group'] == 'CPS') & (df['hour'] == '20:00')].index.values[0])
        ids.append(df[(df['symbol'] == 'RLC') & (df['group'] == 'FCS') & (df['hour'] == '15:00')].index.values[0])
        ids.append(df[(df['symbol'] == 'AST') & (df['group'] == 'CCB') & (df['hour'] == '14:00')].index.values[0])
        ids.append(df[(df['symbol'] == 'AST') & (df['group'] == 'MPG') & (df['hour'] == '14:00')].index.values[0])
        ids.append(df[(df['symbol'] == 'NXS') & (df['group'] == 'MPG') & (df['hour'] == '18:00')].index.values[0])
        ids.append(df[(df['symbol'] == 'DATA') & (df['group'] == 'WCG') & (df['hour'] == '18:00')].index.values[0])
        ids.append(df[(df['symbol'] == 'CTXC') & (df['group'] == 'MPG') & (df['hour'] == '16:00')].index.values[0])
        ids.append(df[(df['symbol'] == 'CVC') & (df['group'] == 'CPS') & (df['hour'] == '20:00')].index.values[0])
        ids.append(df[(df['symbol'] == 'QSP') & (df['group'] == 'CPS') & (df['hour'] == '18:00')].index.values[0])
        ids.append(df[(df['symbol'] == 'OST') & (df['group'] == 'WCG') & (df['hour'] == '18:00')].index.values[0])
        ids.append(df[(df['symbol'] == 'OST') & (df['group'] == 'WCG') & (df['hour'] == '18:00')].index.values[0])
        df.drop(ids, inplace=True)
    return df


def to_timestamp(dt):
    return ccxt_exchange.parse8601(dt.isoformat())


def create_ohlcv_df(data):
    header = ['timestamp', 'open', 'high', 'low', 'close', 'volume']
    df = pd.DataFrame(data, columns=header)
    df['date'] = pd.to_datetime(df['timestamp'], unit='ms', origin='unix')  # convert timestamp to datetime
    return df


def create_orders_df(data):
    keys = ['symbol', 'timestamp', 'datetime', 'side', 'price', 'amount']
    df = pd.DataFrame([[row.get(key) for key in keys] for row in data])  # genius!
    df.columns = keys
    df['volume'] = df['price'] * df['amount']
    return df


def fetch_data(symbol, since, timeframe, exchange, eth=True):
    orders_btc = exchange.fetch_trades(symbol + '/BTC', since)
    if eth:
        orders_eth = exchange.fetch_trades(symbol + '/ETH', since)
    else:  # copy from btc to have the same data-structure
        orders_eth = orders_btc.copy()
    return orders_btc, orders_eth


def download(symbol, start, end, candle_size_str, exchange, eth):
    '''
    Download all the transaction for a given symbol from the start date to the end date
    @param symbol: the symbol of the coin for which download the transactions
    @param start: the start date from which download the transaction
    @param end: the end date from which download the transaction
    '''
    orders_btc = pd.DataFrame();
    orders_eth = pd.DataFrame()
    since = start
    logger.info(
        f'Downloading {symbol} from {datetime.fromtimestamp(start // 1000)} to {datetime.fromtimestamp(end // 1000)}')
    while since < end:
        # logger.info(f'--since: {datetime.fromtimestamp(since//1000)}') #uncomment line for verbose download
        try:
            ord_btc, ord_eth = fetch_data(symbol, since, candle_size_str, exchange, eth)
        except RequestTimeout:
            time.sleep(5)

        min_len = min(len(ord_btc), len(ord_eth))
        if min_len > 0:
            min_ts = min(ord_btc[-1]['timestamp'], ord_eth[-1]['timestamp'])
            latest_ts = min_ts
            if since != latest_ts:
                since = latest_ts
            else:
                since += ten_minutes
            orders_btc = orders_btc.append(create_orders_df(ord_btc))
            orders_eth = orders_eth.append(create_orders_df(ord_eth))
            # ohlcv_btc = ohlcv_btc.append(create_ohlcv_df(candle_btc))
            # ohlcv_eth = ohlcv_eth.append(create_ohlcv_df(candle_eth))
        else:
            since += ten_minutes
    return [orders_btc.drop_duplicates(), orders_eth.drop_duplicates()]


def download_from_exchange(exchange, days_before=4, days_after=4, url=csv_url, candlestick='tiny', path='data/'):
    '''
    Download all the transactions for all the pumps in binance in a given interval
    @param days_before: the number of days before the pump
    @param days_after: the number of days after the pump
    '''
    pd_attempts = get_pd_attempts(csv_url=url)
    c_size_str = candles_sticks[candlestick]['str']
    pd_attempts['dateTime'] = pd_attempts['date'] + ' ' + pd_attempts['hour']
    pd_attempts['pumpTime'] = [datetime.strptime(date, "%Y-%m-%d %H:%M") for date in pd_attempts['dateTime']]
    pd_attempts['firstDate'] = [to_timestamp(date - timedelta(days=days_before)) for date in pd_attempts['pumpTime']]
    pd_attempts['lastDate'] = [to_timestamp(date + timedelta(days=days_after)) for date in pd_attempts['pumpTime']]
    binance_only = pd_attempts[pd_attempts['exchange'] == exchange.name.lower()]
    for t in binance_only.itertuples():  # itertuples is quite fast
        if os.path.exists(path + '/orders_{}_btc_{}'.format(t[1], str(t[6]).replace(':', '.') + '.csv')):
            logger.info(f'{t[1]} already loaded.')
            continue
        try:
            eth = False if t[1] in ['POLY', 'CTXC'] else True  ## doesn't have ./eth data
            all_dfs = download(symbol=t[1], start=t[8], end=t[9], candle_size_str=c_size_str, exchange=exchange,
                               eth=eth)
            df_orders_btc, df_orders_eth = all_dfs
            df_orders_btc.to_csv(path + 'orders_{}_btc_{}'.format(t[1], str(t[6]).replace(':', '.') + '.csv'),
                                 index=False)
            df_orders_eth.to_csv(path + 'orders_{}_eth_{}'.format(t[1], str(t[6]).replace(':', '.') + '.csv'),
                                 index=False)
        except Exception as e:
            logger.error(f'Could not load {t[1]} exception: {e}')


@util.print_runtime
def spotInconsistencies(superPosHours=24, removeDouble=True):
    pd_attempts = get_pd_attempts()
    pd_attempts['dateHour'] = pd_attempts['date'] + ' ' + pd_attempts['hour']
    pd_attempts['datetime'] = [datetime.strptime(strDate, "%Y-%m-%d %H:%M") for strDate in pd_attempts['dateHour']]
    pd_attempts.drop(['dateHour'], axis=1, inplace=True)
    pd_sorted = pd_attempts.sort_values(['datetime'], inplace=False)
    symbolsUnique = pd_sorted.symbol.unique()
    dfDict = {elem: pd.DataFrame for elem in symbolsUnique}
    probDict = {}  # problem dictionary
    for key in dfDict.keys():
        dfDict[key] = pd_sorted[:][pd_sorted['symbol'] == key]
    for key in dfDict.keys():
        dfDict[key]['diff'] = dfDict[key]['datetime'] - dfDict[key]['datetime'].shift(1)
        if removeDouble:
            dfDict[key]['inconsistency'] = [
                1 if timedelta(hours=0, minutes=0.5) < diff < timedelta(hours=superPosHours, minutes=1)
                else 0 for diff in dfDict[key]['diff']]
        else:
            dfDict[key]['inconsistency'] = [1 if diff < timedelta(hours=superPosHours, minutes=1)
                                            else 0 for diff in dfDict[key]['diff']]
        if 1 in dfDict[key].inconsistency.values and len(dfDict[key].index) > 1:
            logger.info(f'There might be problems for {key}')
            # logger.info(dfDict[key])
            probDict[key] = dfDict[key]
    return dfDict, probDict


def selectThroughExchange(probDict):
    selDict = {}
    for key in probDict.keys():
        if exchange_str in probDict[key].exchange.values:
            selDict[key] = probDict[key]
    return selDict


if __name__ == '__main__':
    download_from_exchange(exchange=ccxt_exchange, days_before=7, days_after=7)
    dfDict, probDict = spotInconsistencies(removeDouble=False)
    excProbDict = selectThroughExchange(probDict)
